import { createContext } from "react"

interface APIContext {
    state: any,
    groupCards?: any,
    mutate: (payload: any, method: string) => void
}

export const APIContext = createContext<APIContext>({} as APIContext)
