import React from 'react';
import List from '../components/List';

interface ListPageProps {
  group: string
}

const IncomeExpenseList = ({ group }: ListPageProps) => {

  return (
    <div>
      <List listName={group} />
    </div>
  );
}

export default IncomeExpenseList;