import React from 'react';
import ExpenseGroups from '../components/ExpenseGroups';
import IncomeGroups from '../components/IncomeGroups';

interface GroupListProps {
  group: string
}

const GroupList = ({ group }: GroupListProps) => {

  return (
    <div>
      {group === 'expense_groups' ? <ExpenseGroups /> : <IncomeGroups />}
    </div>
  );
}

export default GroupList;