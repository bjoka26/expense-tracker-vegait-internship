import React from 'react';
import AppTitle from '../components/AppTitle';
import Expenses from '../components/Expenses';
import Incomes from '../components/Incomes';

const Dashboard = () => {
  return (
    <div className='container-tracker'>
      <AppTitle title='Expense Tracker' />
      <div className='dashboard-container'>
        <Expenses isDashboard={true} />
        <Incomes isDashboard={true} />
      </div>
    </div>
  );
}

export default Dashboard;