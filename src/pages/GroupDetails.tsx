import React from 'react';
import { useParams } from 'react-router-dom';
import AppTitle from '../components/AppTitle';
import Expenses from '../components/Expenses';
import Incomes from '../components/Incomes';
import useFetch from '../hooks/useFetch';

interface GroupDetailsProps {
  group: string
}

const GroupDetails = ({ group }: GroupDetailsProps) => {
  const { groupId } = useParams();
  const { state } = useFetch(`${group}/${groupId}`);
  const groupName = state.data.name;

  return (
    <div>
      <AppTitle title='Expense Tracker' />
      <div>
        <AppTitle title={`${state.data.name} group details:`} />
        <h4>Details:</h4>
      </div>
      <div>
        <p>{state.data.description}</p>
      </div>
      {group === 'expense_groups' ? <Expenses groupName={groupName} /> : <Incomes groupName={groupName} />}
    </div>
  );
}

export default GroupDetails;