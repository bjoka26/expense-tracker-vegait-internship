import React, { useState } from 'react'
import useFetch from '../hooks/useFetch';
import AddExpenseModal from './AddExpenseModal';
import TrackerContainer from './TrackerContainer';
import AppButton from './AppButton';
import EditModal from './EditModal';
import { APIContext } from '../utils/APIContext';
import { GROUP } from '../types/enums';

interface ExpenseProps {
    groupName?: string
    isDashboard?: boolean
}

const Expenses = ({ groupName = '', isDashboard = false }: ExpenseProps) => {
    const { state, mutate, invalidate } = useFetch('expenses?_limit=5');
    const [modalToggler, setModalToggler] = useState(false);
    const [editModalToggler, setEditModalToggler] = useState(false);
    const [pageNo, setPageNo] = useState(1);
    const details = true;
    const [editInfo, setEditInfo] = useState({
        description: '',
        amount: 0,
        group: '',
        id: 0
    });
    let groupCards = state.data;
    if (groupName) {
        groupCards = state.data.filter(item => {
            return item.group === groupName
        });
        if (groupCards.length > 4) {
            groupCards.splice(state.data.length - 6, state.length - 1);
        }
    }

    const toggleModal = () => setModalToggler(!modalToggler);

    const modalController = (id: number, description: string | number, amount: number) => {
        setEditModalToggler(true);

        setEditInfo({
            description: description.toString(),
            amount: amount,
            group: GROUP.EXPENSES,
            id: id
        })
    }

    const fetchContextValue = {
        state,
        groupCards,
        mutate
    }

    return (
        <APIContext.Provider value={fetchContextValue}>
            <div className='group-container'>
                <TrackerContainer isDashboard={isDashboard} details={details} setPageNo={setPageNo} buttonName="Expense group list" title="Total Expenses" group={GROUP.EXPENSE_GROUPS} modalController={modalController} />
                <AppButton name="Add Expense" onPress={toggleModal} />
                {modalToggler ? <AddExpenseModal groupType={GROUP.EXPENSE_GROUPS} setModalToggler={setModalToggler} onPress={toggleModal} /> : null}
                {editModalToggler ? <EditModal editInfo={editInfo} setEditModalToggler={setEditModalToggler} /> : null}
            </div>
        </APIContext.Provider>
    )
}

export default Expenses