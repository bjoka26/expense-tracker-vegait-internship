import React, { useContext } from 'react';
import { HTTP_METHOD } from '../types/enums';
import { IncomeExpense } from '../types/types';
import { APIContext } from '../utils/APIContext';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { GROUP } from '../types/enums';

interface TableProps {
    incomeExpenseData: IncomeExpense[],
    modalController: (id: number, description: string | number, amount: number) => void
    group?: string;
}

const Table = ({ incomeExpenseData, modalController, group = '' }: TableProps) => {
    const { mutate } = useContext(APIContext);

    return (
        <div>
            {incomeExpenseData?.map((info: IncomeExpense) => {
                return <div className='card'>
                    <div className='card-info'>
                        <h4>{info.group}</h4>
                        <p>{info.date}</p>
                    </div>
                    <p>${group === GROUP.EXPENSE_GROUPS ? '-' : '+'}{info.amount}</p>
                    <div className='card-buttons'>
                        <p><EditIcon sx={{ cursor: 'pointer' }} onClick={() => modalController(info.id, info.description, +info.amount)} /></p>
                        <p><DeleteIcon sx={{ cursor: 'pointer' }} onClick={() => mutate(info.id, HTTP_METHOD.DELETE)} /></p>
                    </div>
                </div>
            })}
        </div>
    )
}

export default Table