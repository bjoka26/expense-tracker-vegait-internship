import React, { useState } from 'react'
import useFetch from '../hooks/useFetch';
import { APIContext } from '../utils/APIContext';
import AddExpenseModal from './AddExpenseModal';
import AppButton from './AppButton';
import EditModal from './EditModal';
import TrackerContainer from './TrackerContainer';

interface ListProps {
    listName: string
}

const List = ({ listName }: ListProps) => {
    const [modalToggler, setModalToggler] = useState<boolean>(false);
    const [editModalToggler, setEditModalToggler] = useState(false);
    const [pageNo, setPageNo] = useState(1);
    const { state, mutate, invalidate } = useFetch(`${listName}?_page=${pageNo}&_limit=7`);
    const groupType = listName === 'expenses' ? 'expense_groups' : 'income_groups';
    const [editInfo, setEditInfo] = useState({
        description: '',
        amount: 0,
        group: '',
        id: 0
    });

    const toggleModal = () => setModalToggler(!modalToggler);

    const modalController = (id: number, description: string | number, amount: number) => {
        setEditModalToggler(true);

        setEditInfo({
            description: description.toString(),
            amount: amount,
            group: listName,
            id: id
        })
    }

    const fetchContextValue = {
        state,
        mutate
    }

    return (
        <APIContext.Provider value={fetchContextValue}>
            <div>
                <TrackerContainer pageNo={pageNo} setPageNo={setPageNo} modalController={modalController} title={listName} buttonName='Dashobard' group='' />
                <AppButton name="Add New" onPress={toggleModal} />
                {modalToggler ? <AddExpenseModal groupType={groupType} setModalToggler={setModalToggler} onPress={toggleModal} /> : null}
                {editModalToggler ? <EditModal editInfo={editInfo} setEditModalToggler={setEditModalToggler} /> : null}
            </div>
        </APIContext.Provider>
    )
}

export default List