import React, { SetStateAction, useContext, useEffect, useState } from 'react'
import { HTTP_METHOD } from '../types/enums';
import { APIContext } from '../utils/APIContext';
import AppButton from './AppButton'
import AppInput from './inputs/AppInput';
import { Modal, ModalBackDrop, ModalBody, ModalFooter, ModalHeader } from './modal/Modal';

interface EditProps {
    editInfo: DataType,
    setEditModalToggler: React.Dispatch<SetStateAction<boolean>>
}

type DataType = {
    description: string,
    amount: number,
    group?: string,
    id?: number
}

const EditModal = ({ setEditModalToggler, editInfo }: EditProps) => {
    const { mutate } = useContext(APIContext)

    const [inputData, setInputData] = useState<DataType>({
        description: '',
        amount: 0
    });
    const [oldInputData, setOldInputData] = useState<DataType>({
        description: '',
        amount: 0
    });

    const checkForChange = (data: DataType) => {
        if (data.description.trim() !== oldInputData.description || data.amount !== oldInputData.amount) {
            return true;
        } else {
            return false;
        }
    }

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInputData((prevInput) => {
            return {
                ...prevInput,
                [e.target.id]: e.target.value
            }
        });
    }

    const sendData = () => {
        if (checkForChange(inputData)) {
            mutate({ id: editInfo.id, data: inputData }, HTTP_METHOD.PATCH);
        }
    }

    const getCardInfo = () => {
        setInputData({
            description: editInfo.description,
            amount: editInfo.amount
        });
        setOldInputData({
            description: editInfo.description,
            amount: editInfo.amount
        });
    }

    useEffect(() => {
        getCardInfo();
    }, [])


    return (
        <ModalBackDrop setModalToggler={setEditModalToggler} >
            <Modal>
                <ModalHeader>
                    <h3>Edit Expense/Income</h3>
                </ModalHeader>
                <ModalBody>
                    <form>
                        <div>
                            <AppInput title='Description:' id="description" value={inputData.description} onChange={handleInput} />
                        </div>
                        <div>
                            <AppInput title='Amount:' id="amount" value={inputData.amount} onChange={handleInput} />
                        </div>
                    </form>
                </ModalBody>
                <ModalFooter>
                    <AppButton name="Save" onPress={sendData} />
                    <AppButton name="Cancel" onPress={() => setEditModalToggler(false)} />
                </ModalFooter>
            </Modal>
        </ModalBackDrop>
    );
}

export default EditModal;
