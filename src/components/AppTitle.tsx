import React from 'react';

interface TitleProps {
  title: string
}

const AppTitle = ({ title }: TitleProps) => {
  return (
    <h3 className='app-title'>{title}</h3>
  );
}

export default AppTitle;