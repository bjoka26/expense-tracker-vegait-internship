import React, { useState } from 'react'
import useFetch from '../hooks/useFetch';
import AddExpenseModal from './AddExpenseModal';
import TrackerContainer from './TrackerContainer';
import AppButton from './AppButton';
import EditModal from './EditModal';
import { APIContext } from '../utils/APIContext';
import { GROUP } from '../types/enums';
import { IncomeExpense } from '../types/types';

interface IncomeProps {
    groupName?: string
    isDashboard?: boolean
}

const Incomes = ({ groupName = '', isDashboard = false }: IncomeProps) => {
    const { state, mutate, invalidate } = useFetch('incomes?_limit=5');
    const [pageNo, setPageNo] = useState(1);
    const [modalToggler, setModalToggler] = useState(false);
    const [editModalToggler, setEditModalToggler] = useState(false);
    const details = true;
    const [editInfo, setEditInfo] = useState({
        description: '',
        amount: 0,
        group: '',
        id: 0
    });

    let groupCards = state.data;
    if (groupName) {
        groupCards = state.data.filter(item => {
            return item.group === groupName
        });
        if (groupCards.length > 4) {
            groupCards.splice(state.data.length - 6, state.length - 1);
        }
    }

    const toggleModal = () => setModalToggler(!modalToggler);

    const modalController = (id: number, description: string | number, amount: number) => {
        setEditModalToggler(true);

        setEditInfo({
            description: description.toString(),
            amount: amount,
            group: GROUP.INCOMES,
            id: id
        });
    }

    const FetchContextValue = {
        state,
        groupCards,
        mutate
    }

    return (
        <APIContext.Provider value={FetchContextValue}>
            <div className='group-container'>
                <TrackerContainer isDashboard={isDashboard} details={details} setPageNo={setPageNo} buttonName="Income group list" title="Total Incomes" group={GROUP.INCOME_GROUPS} modalController={modalController} />
                <AppButton name="Add Income" onPress={toggleModal} />
                {modalToggler ? <AddExpenseModal groupType={GROUP.INCOME_GROUPS} setModalToggler={setModalToggler} onPress={toggleModal} /> : null}
                {editModalToggler ? <EditModal editInfo={editInfo} setEditModalToggler={setEditModalToggler} /> : null}
            </div>
        </APIContext.Provider>
    )
}

export default Incomes