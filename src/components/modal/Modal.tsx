import React from 'react';
import styles from '../modal/Modal.module.css';

export const ModalBackDrop = ({ children, setModalToggler }: { children: React.ReactNode, setModalToggler: React.Dispatch<React.SetStateAction<boolean>> }) => {
  const handleModalBackDropClose = (e: React.MouseEvent<HTMLDivElement>) => {
    const target = e.target as HTMLDivElement;
    if (target.id === 'backdrop') {
      setModalToggler(false);
    }
  }
  return (
    <div id="backdrop" className={styles.modal} onClick={handleModalBackDropClose}>
      {children}
    </div>
  )
}

export const Modal = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className={styles.modal_box}>
      {children}
    </div>
  )
}

export const ModalHeader = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      {children}
    </div>
  )
}

export const ModalBody = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      {children}
    </div>
  )
}

export const ModalFooter = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className={styles.modal_footer}>
      {children}
    </div>
  )
}