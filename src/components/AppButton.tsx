import React from 'react';

interface ButtonProps {
  color?: any,
  name: string,
  onPress?: (e: React.MouseEvent<HTMLButtonElement>) => void
}

const AppButton = ({ color, name, onPress }: ButtonProps) => {
  return (
    <button className='app-button' color={color} onClick={onPress}>{name}</button>
  );
}

export default AppButton;