import React, { SetStateAction, useContext } from 'react';
import AppButton from './AppButton'
import { Link } from "react-router-dom"
import Pagination from '@mui/material/Pagination';
import { APIContext } from '../utils/APIContext';
import { DataGrid, GridToolbar } from '@mui/x-data-grid';
import { HTTP_METHOD } from '../types/enums';
import Table from './Table';
import GroupsTable from './GroupsTable';

interface TrackerProps {
    details?: boolean,
    buttonName: string,
    title: string,
    group?: string,
    groupType?: string,
    pageNo?: number,
    setPageNo: React.Dispatch<SetStateAction<number>>,
    isDashboard?: boolean,
    modalController: (id: number, description: string | number, amount: number) => void
}

const TrackerContainer = ({ title, buttonName, modalController, group = '', pageNo, setPageNo, groupType = '', details = false, isDashboard = false }: TrackerProps) => {
    const { state, groupCards, mutate } = useContext(APIContext);
    const noOfPages = Math.ceil(+state.length / 7);
    const linkToList = group.toLowerCase() === 'expense_groups' ? 'expenses' : 'incomes';
    let cardData = state.data;

    const handlePageChange = (event: React.ChangeEvent<unknown>, value: number) => {
        setPageNo(value);
    }

    if (details) {
        cardData = groupCards
    }

    return (
        <div className='tracker-box'>
            <div className='card-header'>
                <h4>{title}</h4>
                <Link to={`/${group}`}>
                    <AppButton name={buttonName} />
                </Link>
            </div>
            <div className='data-table'>
                {groupType ? <GroupsTable groupData={cardData} /> : <Table group={group} incomeExpenseData={cardData} modalController={modalController} />}
                {!isDashboard && <Pagination count={noOfPages} page={pageNo} onChange={handlePageChange} />}
            </div>
            {pageNo
                ?
                null
                :
                (<Link to={`/${linkToList}`}>
                    <AppButton name='View All' />
                </Link>)}
        </div>
    );
}

export default TrackerContainer;
