import React from 'react'
import styles from '../../pages/Dashboard.module.css';

interface InputProps {
  title: string
  id: string,
  value: string | number,
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const AppInput = ({ title, id, value, onChange }: InputProps) => {
  return (
    <div>
      <p>{title}</p>
      <input placeholder={title} className={styles.form__input} type="text" id={id} value={value} onChange={(e) => onChange(e)} />
    </div>
  )
}

export default AppInput