import React, { useContext } from 'react';
import { HTTP_METHOD } from '../types/enums';
import { Groups } from '../types/types';
import { APIContext } from '../utils/APIContext';
import DeleteIcon from '@mui/icons-material/Delete';

interface TableProps {
    groupData: Groups[],
}

const Table = ({ groupData }: TableProps) => {
    const { mutate } = useContext(APIContext);
    return (
        <div>
            {groupData?.map((info: Groups) => {
                return <div className='card'>
                    <div className='card-info'>
                        <h4>{info.name}</h4>
                    </div>
                    <p>{info.description}</p>

                    <div className='card-buttons'>
                        <p><DeleteIcon sx={{ cursor: 'pointer' }} onClick={() => mutate(info.id, HTTP_METHOD.DELETE)} /></p>
                    </div>
                </div>
            })}
        </div>
    )
}

export default Table