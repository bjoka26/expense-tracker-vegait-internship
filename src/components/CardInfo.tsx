import { Link } from 'react-router-dom';
import { Groups, IncomeExpense } from '../types/types';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import React from 'react';
import useFetch from '../hooks/useFetch';

interface CardProps {
    group: string,
    incomeExpenseData: IncomeExpense | Groups,
    modalController: (id: number, description: string | number, amount: number) => void,
}

const CardInfo = ({ incomeExpenseData, modalController, group }: CardProps) => {
    const { mutate } = useFetch(group);
    const getDataArray = Object.values(incomeExpenseData);
    const showCardInfo = getDataArray.map((value) => {
        return <p className='card-item'>{value}</p>
    });

    return (
        <div className='card-container'>
            {group.includes('_groups') ? (<Link className='router-link' to={`/${group}/${getDataArray[0]}`}>
                {showCardInfo}
            </Link>) : showCardInfo}
            <EditIcon onClick={() => modalController(+getDataArray[0], getDataArray[1], +getDataArray[2])} />
            <DeleteIcon onClick={() => mutate(getDataArray[0], 'DELETE')} />
        </div>
    );
}

export default CardInfo;