import React, { useContext, useState } from 'react'
import { ModalBackDrop, Modal, ModalHeader, ModalBody, ModalFooter } from '../components/modal/Modal';
import AppButton from './AppButton';
import AppInput from './inputs/AppInput';
import { HTTP_METHOD } from '../types/enums';
import { APIContext } from '../utils/APIContext';

interface GroupAddProps {
    title: string
    group?: string,
    onPress: () => void
    setModalToggler: React.Dispatch<React.SetStateAction<boolean>>
}

const GroupAddModal = ({ onPress, setModalToggler, group = '', title }: GroupAddProps) => {
    const { mutate } = useContext(APIContext);
    const [input, setInput] = useState({
        name: '',
        description: ''
    });

    const validateInput = (input.description === '' || input.name === '')

    const addGroup = async (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        const newGroup = {
            id: null,
            name: input.name,
            description: input.description,
        };
        if (validateInput) {
            return;
        }
        mutate(newGroup, HTTP_METHOD.POST);
    }

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInput((prev) => {
            return {
                ...prev,
                [e.target.id]: e.target.value
            }
        });
    }

    return (
        <ModalBackDrop setModalToggler={setModalToggler} >
            <Modal>
                <ModalHeader>
                    <h3>{title}</h3>
                </ModalHeader>
                <form>
                    <ModalBody>
                        <AppInput title='Name:' id="name" value={input.name} onChange={handleInput} />
                        <AppInput title='Description:' id="description" value={input.description} onChange={handleInput} />
                    </ModalBody>
                    <ModalFooter>
                        <AppButton name="Save" onPress={addGroup} />
                        <AppButton name="Cancel" onPress={onPress} />
                    </ModalFooter>
                </form>
            </Modal>
        </ModalBackDrop>
    )
}

export default GroupAddModal;