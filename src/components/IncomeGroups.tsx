import React, { useState } from 'react'
import useFetch from '../hooks/useFetch';
import { GROUP } from '../types/enums';
import { IncomeExpense } from '../types/types';
import { APIContext } from '../utils/APIContext';
import AppButton from './AppButton';
import GroupAddModal from './GroupAddModal';
import TrackerContainer from './TrackerContainer';

interface IncomeGroupProps { }

const IncomeGroups = (props: IncomeGroupProps) => {
  const [modalToggler, setModalToggler] = useState(false);
  const [editModalToggler, setEditModalToggler] = useState(false);
  const [description, setDescription] = useState('');
  const [amount, setAmount] = useState(0);
  const [pageNo, setPageNo] = useState(1);
  const groupCards: IncomeExpense[] = [];
  const { state, mutate } = useFetch(`income_groups?_page=${pageNo}&_limit=7`);

  const toggleModal = () => setModalToggler(!modalToggler);

  const modalController = (description: string | number, amount: string | number) => {
    setEditModalToggler(true);
    setDescription(description.toString());
    setAmount(+amount);
  }

  return (
    <APIContext.Provider value={{ state, groupCards, mutate }}>
      <div>
        <TrackerContainer pageNo={pageNo} setPageNo={setPageNo} modalController={modalController} title='Income Groups' buttonName='Dashobard' groupType={GROUP.INCOME_GROUPS} />
        <AppButton name="Add Group" onPress={toggleModal} />
        {modalToggler ? <GroupAddModal setModalToggler={setModalToggler} onPress={toggleModal} title='Add Income Group' group={GROUP.INCOME_GROUPS} /> : null}
      </div>
    </APIContext.Provider>
  )
}

export default IncomeGroups;