import React, { useContext, useState } from 'react'
import { ModalBackDrop, Modal, ModalHeader, ModalBody, ModalFooter } from '../components/modal/Modal';
import AppButton from './AppButton';
import AppInput from './inputs/AppInput';
import ModalSelect from './ModalSelect';
import { HTTP_METHOD } from '../types/enums';
import { APIContext } from '../utils/APIContext';

interface AddExpenseIncomeProps {
    groupType: string,
    onPress: () => void,
    setModalToggler: React.Dispatch<React.SetStateAction<boolean>>
}

const AddExpenseModal = ({ onPress, setModalToggler, groupType }: AddExpenseIncomeProps) => {
    const { mutate } = useContext(APIContext);
    const [getSelect, setGetSelect] = useState('');
    const [input, setInput] = useState({
        description: '',
        amount: 0
    });
    const validateInput = (input.description === '' || input.amount === 0)

    const addExpenseOrIncome = (e: React.MouseEvent<HTMLButtonElement>) => {
        e.preventDefault();
        const newIncomeExpense = {
            id: null,
            description: input.description,
            amount: +input.amount,
            group: getSelect,
            date: new Date('2021-04-08').toISOString().split('T')[0]
        };
        if (validateInput) {
            return;
        }

        mutate(newIncomeExpense, HTTP_METHOD.POST);
        setInput({
            description: '',
            amount: 0
        })
    }

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        setInput((prev) => {
            return {
                ...prev,
                [e.target.id]: e.target.value
            }
        })
    }

    return (
        <ModalBackDrop setModalToggler={setModalToggler} >
            <Modal>
                <ModalHeader>
                    <h3>Add {groupType === 'expense_groups' ? 'Expenses' : 'Incomes'}</h3>
                </ModalHeader>
                <form>
                    <ModalBody>
                        <AppInput title='Description:' id="description" value={input.description} onChange={handleInput} />
                        <AppInput title='Amount:' id="amount" value={input.amount} onChange={handleInput} />
                        <ModalSelect value={getSelect} id="group" groupType={groupType} onChange={(e) => setGetSelect(e.target.value)} />
                    </ModalBody>
                    <ModalFooter>
                        <AppButton name="Save" onPress={addExpenseOrIncome} />
                        <AppButton name="Cancel" onPress={onPress} />
                    </ModalFooter>
                </form>
            </Modal>
        </ModalBackDrop>
    )
}

export default AddExpenseModal