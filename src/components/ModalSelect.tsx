import React from 'react'
import useFetch from '../hooks/useFetch';

interface SelectProps {
    value: string,
    id: string,
    groupType: string,
    onChange: (e: React.ChangeEvent<HTMLSelectElement>) => void
}

const ModalSelect = ({ value, onChange, groupType, id }: SelectProps) => {
    const { state } = useFetch(groupType);

    return (
        <select className='select' name="choose-group" value={value} id={id} onChange={(e) => onChange(e)} >
            <option value="default">Select Something</option>
            {groupType === "expense_groups" ?
                state.data?.map((groupName: any) => {
                    return <option value={groupName.name}>{groupName.name}</option>
                }) :
                state.data?.map((groupName: any) => {
                    return <option value={groupName.name}>{groupName.name}</option>
                })
            }
        </select>
    )
}

export default ModalSelect