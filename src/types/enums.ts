export enum GROUP {
    EXPENSE_GROUPS = 'EXPENSE_GROUPS',
    INCOME_GROUPS = 'INCOME_GROUPS',
    EXPENSES = 'EXPENSES',
    INCOMES = 'INCOMES'
}

export enum HTTP_METHOD {
    DELETE = 'DELETE',
    POST = 'POST',
    PATCH = 'PATCH',
    GET = 'GET'
}