export type IncomeExpense = {
    id: number,
    description: string,
    amount: number,
    group: string,
    date: string
}

export type Groups = {
    id: number,
    description: string,
    name: string,
}

export type Group = {
    expenses: IncomeExpense[],
    incomes: IncomeExpense[],
    income_groups: Groups[],
    expense_groups: Groups[]
}
