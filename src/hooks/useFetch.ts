import React from 'react';
import { useEffect, useReducer } from "react";
import axios from "axios";

const ACTIONS = {
    API_REQUEST: "api-request",
    FETCH_DATA: "fetch-data",
    DELETE_DATA: "delete-data",
    ADD_DATA: "add-data",
    ERROR: "error"
};

const MUTATE = {
    POST: 'POST',
    PATCH: 'PATCH',
    DELETE: 'DELETE',
}

export const initialState = {
    data: [],
    loading: false,
    error: null,
    length: 0
}

const reducer = (state: any, { type, payload }: any) => {
    switch (type) {
        case ACTIONS.API_REQUEST:
            return { ...state, data: [], loading: true }

        case ACTIONS.FETCH_DATA:
            return { ...state, data: payload.data, loading: true, length: payload.headers['x-total-count'] }

        default:
            return state;
    }
}

const useFetch = (route: string) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const instance = axios.create({
        baseURL: process.env.REACT_APP_API_URL,
    });    

    const getData = async () => {        
        dispatch({ type: ACTIONS.API_REQUEST });
        try {
            const request = await instance.get(route);
            dispatch({ type: ACTIONS.FETCH_DATA, payload: request })
        } catch (error) {
            console.log(error);

            dispatch({ type: ACTIONS.FETCH_DATA, payload: error })
        }
    }

    const invalidate = () => {
        getData();
    }

    const mutate = async (payload: any, method: string) => {
        switch (method) {
            case MUTATE.DELETE:
                try {
                    const editedRoute = route.split('?')[0];

                    await instance.delete(`${editedRoute}/${payload}`)
                    getData();
                    return;
                } catch (error) {
                    console.log(error);
                    return;
                }
            case MUTATE.POST:
                try {
                    const editedRoute = route.split('?')[0];

                    await instance.post(editedRoute, payload);
                    invalidate();
                    return;
                } catch (error) {
                    return;
                }
            case MUTATE.PATCH:
                try {
                    const editedRoute = route.split('?')[0];

                    await instance.patch(`${editedRoute}/${payload.id}`, payload.data);
                    invalidate();
                    return;
                } catch (error) {
                    return;
                }
            default:
                return;
        }
    }

    useEffect(() => {
        getData();
    }, [route])

    return { state, mutate, invalidate };

}

export default useFetch;