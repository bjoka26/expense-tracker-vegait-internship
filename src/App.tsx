import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Dashboard from './pages/Dashboard';
import GroupDetails from './pages/GroupDetails';
import GroupList from './pages/GroupList';
import IncomeExpenseList from './pages/IncomeExpenseList';
import NotFound from './pages/NotFound';

function App() {
  return (
    <BrowserRouter>
      <div className='App'>
        <Routes>
          <Route path="/" element={<Dashboard />} />
          <Route path="/expense_groups" element={<GroupList group="expense_groups" />} />
          <Route path="/income_groups" element={<GroupList group="income_groups" />} />
          <Route path="/expense_groups/:groupId" element={<GroupDetails group='expense_groups' />} />
          <Route path="/income_groups/:groupId" element={<GroupDetails group='income_groups' />} />
          <Route path="/expenses" element={<IncomeExpenseList group='expenses' />} />
          <Route path="/incomes" element={<IncomeExpenseList group='incomes' />} />
          <Route path="/expenses/:id" element={<IncomeExpenseList group='expenses' />} />
          <Route path="/incomes/:id" element={<IncomeExpenseList group='incomes' />} />

          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
